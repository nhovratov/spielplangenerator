<?php
session_start();

$torearray = $_GET['tore'];
$gruppenMatchups = $_SESSION['matchups'];
$gruppenzahl = count($gruppenMatchups);
$spielerhalbfinale = $_SESSION['spielernamen'];

//------------------------------------------------------------------//
// Eine für $gruppenMatchups passende $gruppenTore Variable erstellen //
//--------------------------------------------------------------------//

for ($i = 1, $toreNum = 0; $i <= $gruppenzahl; $i++) {

    for ($n = 1; $n <= count($gruppenMatchups['Gruppe' . $i]); $n++) {

        if (count($gruppenMatchups["Gruppe" . $i]["Spiel" . $n]) == 4) {

            $gruppenTore["Gruppe" . $i]["Spiel" . $n] = array(
                $torearray[$toreNum],
                $torearray[$toreNum + 1],
                $torearray[$toreNum + 2],
                $torearray[$toreNum + 3]
            );
            $toreNum += 4;
        } else {
            $gruppenTore["Gruppe" . $i]["Spiel" . $n] = array(
                $torearray[$toreNum],
                $torearray[$toreNum + 1]
            );
            $toreNum += 2;
        }

    }
}

//echo '<pre>';
//print_r($gruppenMatchups);
//print_r($gruppenTore);
//echo '</pre>';

//--- Toranzahl der Spieler ----//
$gruppenaufteilung = $_SESSION['gruppenaufteilung'];
for ($groupNum = 1; $groupNum <= $gruppenzahl; $groupNum++) {

    for ($n = 1; $n <= count($gruppenMatchups["Gruppe" . $groupNum]); $n++) {
        if (count($gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n]) == 4) {


            for ($p = 0; $p < 4; $p++) {

                $spielername = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n][$p];
                $spielerTore['Gruppe' . $groupNum][$spielername] += $gruppenTore['Gruppe' . $groupNum]["Spiel" . $n][$p];

            }
        } else {
            for ($p = 0; $p < 2; $p++) {

                $spielername = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n][$p];
                $spielerTore['Gruppe' . $groupNum][$spielername] += $gruppenTore['Gruppe' . $groupNum]["Spiel" . $n][$p];
            }
        }


    }
}

//echo '<pre>';
//print_r($spielerTore);
//echo '</pre>';

//--- Gegentore der Spieler ----//
for ($groupNum = 1; $groupNum <= $gruppenzahl; $groupNum++) {

    for ($n = 1; $n <= count($gruppenMatchups["Gruppe" . $groupNum]); $n++) {

        if (count($gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n]) == 4) {

            for ($p = 0; $p < 3; $p += 2) {
                $spielername1 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n][$p];
                $spielername2 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n][$p + 1];
                $spielerNegativ['Gruppe' . $groupNum][$spielername1] -= $gruppenTore['Gruppe' . $groupNum]["Spiel" . $n][$p + 1];
                $spielerNegativ['Gruppe' . $groupNum][$spielername2] -= $gruppenTore['Gruppe' . $groupNum]["Spiel" . $n][$p];
            }
        } else {
            $i = 0;
            do {
                $spielername1 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n][0];
                $spielername2 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n][1];
                $spielerNegativ['Gruppe' . $groupNum][$spielername1] -= $gruppenTore['Gruppe' . $groupNum]["Spiel" . $n][1];
                $spielerNegativ['Gruppe' . $groupNum][$spielername2] -= $gruppenTore['Gruppe' . $groupNum]["Spiel" . $n][0];
                $i++;
            } while ($i < 1);

        }

    }

}

//echo '<pre>';
//print_r($spielerNegativ);
//echo '</pre>';

//--------------------------------------/////
// Berechnung der Punkte der Spieler    ////
//-------------------------------------////
if (isset($_POST['final'])) {
    for ($i = 0; $i < 4; $i += 2) {
        if ($torearray[$i] > $torearray[$i + 1]) {
            $spielerPunkte['Gruppe1'][$spielerhalbfinale[$i]] += 3;
            $spielerPunkte['Gruppe1'][$spielerhalbfinale[$i + 1]] += 0;

        } else {
            $spielerPunkte['Gruppe1'][$spielerhalbfinale[$i]] += 0;
            $spielerPunkte['Gruppe1'][$spielerhalbfinale[$i + 1]] += 3;
        }
    }
} else {
    for ($groupNum = 1; $groupNum <= $gruppenzahl; $groupNum++) {

        for ($n = 1; $n <= count($gruppenMatchups['Gruppe' . $groupNum]); $n++) {

            if (count($gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n]) < 4) {
                $p = 1;
                $spieler1 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n][0];
                $spieler2 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n][1];
                $tor1 = $gruppenTore['Gruppe' . $groupNum]['Spiel' . $n][0];
                $tor2 = $gruppenTore['Gruppe' . $groupNum]['Spiel' . $n][1];

                do {

                    if ($tor1 > $tor2) {

                        $spielerPunkte['Gruppe' . $groupNum][$spieler1] += 3;
                        $spielerPunkte['Gruppe' . $groupNum][$spieler2] += 0;

                    } else if ($tor1 < $tor2) {

                        $spielerPunkte['Gruppe' . $groupNum][$spieler1] += 0;
                        $spielerPunkte['Gruppe' . $groupNum][$spieler2] += 3;

                    } else {

                        $spielerPunkte['Gruppe' . $groupNum][$spieler1] += 1;
                        $spielerPunkte['Gruppe' . $groupNum][$spieler2] += 1;
                    }
                    $p++;
                } while ($p < 1);
            } else {
                for ($i = 0; $i < 3; $i += 2) {

                    $spieler1 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n][$i];
                    $spieler2 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $n][$i + 1];
                    $tor1 = $gruppenTore['Gruppe' . $groupNum]['Spiel' . $n][$i];
                    $tor2 = $gruppenTore['Gruppe' . $groupNum]['Spiel' . $n][$i + 1];

                    if ($tor1 > $tor2) {

                        $spielerPunkte['Gruppe' . $groupNum][$spieler1] += 3;
                        $spielerPunkte['Gruppe' . $groupNum][$spieler2] += 0;

                    } else if ($tor1 < $tor2) {

                        $spielerPunkte['Gruppe' . $groupNum][$spieler1] += 0;
                        $spielerPunkte['Gruppe' . $groupNum][$spieler2] += 3;

                    } else {

                        $spielerPunkte['Gruppe' . $groupNum][$spieler1] += 1;
                        $spielerPunkte['Gruppe' . $groupNum][$spieler2] += 1;
                    }
                }
            } // end condition

        } // nächster Spiel
    } // nächste Gruppe
}


// Tordifferenz berechnen
for ($groupNum = 1; $groupNum <= $gruppenzahl; $groupNum++) {
    foreach ($spielerPunkte['Gruppe' . $groupNum] as $name => $punkte) {
        $tordiff['Gruppe' . $groupNum][] = $spielerTore['Gruppe' . $groupNum][$name] + $spielerNegativ['Gruppe' . $groupNum][$name];
    }
}
// Nach Punkte und Tordifferenz sortieren.
for ($groupNum = 1; $groupNum <= $gruppenzahl; $groupNum++) {
    array_multisort($spielerPunkte['Gruppe' . $groupNum], SORT_DESC, $tordiff['Gruppe' . $groupNum], SORT_DESC);
}
//echo'<pre>';
//print_r($spielerPunkte);
//print_r($tordiff);
//echo'</pre>';

        for ($groupNum = 1; $groupNum <= $gruppenzahl; $groupNum++) {
            echo '
                <div class="container">
                        <div class="well table-well">
                            <table class="auswertung table table-responsive table-hover">
                            <caption>Gruppe' . $groupNum . '</caption>
                                <thead>
                                    <tr>
                                        <th>Platzierung</th>
                                        <th>Spielername</th>
                                        <th>Punktzahl</td>
                                        <th>Tordifferenz</th>
                                    </tr>
                                </thead>';
            $i = 1;
            $index = 0;
            foreach ($spielerPunkte['Gruppe' . $groupNum] as $name => $punkte) {
                echo "<tr>";
                echo "<td>" . $i . ".</td>
                      <td>" . $name . "</td>
                      <td>" . $punkte . "</td>
                      <td>" . $tordiff['Gruppe' . $groupNum][$index] . "</td>
                      </tr>";
                $i++;
                $index++;
            }

            echo '
                    </table>
                    </div>
        </div>';
        }
        ?>
        <form class="center" action="Tabelle.php" method="post">
            <?php
            if (!isset($_POST['ergebnis'])) {

                $finale = "Halbfinale";
                $post = "semifinal";
                $count = 4;
                if (isset($_POST['final'])) {
                    $finale = "Finale";
                    $post = "final";
                    $count = 2;
                }
                for ($groupNum = 1; $groupNum <= $gruppenzahl; $groupNum++) {
                    $i = 0;
                    foreach ($spielerPunkte['Gruppe' . $groupNum] as $name => $punkte) {
                        $spieler[] = $name;
                        if (++$i == 2) {
                            break;
                        }
                    }
                }
                for ($i = 0; $i < $count; $i++) {

                    echo ' <input type="hidden" name="spieler[]" value="' . $spieler[$i] . '">';
                }

                echo '<input type="submit" name="' . $post . '" value="' . $finale . '">';
            }
            ?>
        </form>
        <div class="center">
            <a href="<?php $_SERVER['PHP_SELF']?>?id=0">
                <button>Zurück zur Startseite</button>
            </a>
        </div>
    </div><!-- ende startseite -->
    </body>

    </html>

<?php
require_once("db_daten.php");

//Punkte vom Finale
if (isset($_POST['ergebnis'])) {

//Spiele in die Datenbank schreiben.

    for ($p = 0; $p < 4; $p += 2) {

        $name1 = $spielerhalbfinale[0];
        $name2 = $spielerhalbfinale[1];
        $tor1 = $torearray[0];
        $tor2 = $torearray[1];
        $mysqli->query("INSERT INTO gamesRecord (spieler1, spieler2, toreSpieler1, toreSpieler2, spieltyp) VALUES('$name1','$name2', $tor1, $tor2, 'gewertet')");

    }


}
//Punkte vom Halbfinale
if (isset($_POST['final'])) {

//Spiele in die Datenbank schreiben.

    for ($p = 0; $p < 4; $p += 2) {

        $name1 = $spielerhalbfinale[$p];
        $name2 = $spielerhalbfinale[$p + 1];
        $tor1 = $torearray[$p];
        $tor2 = $torearray[$p + 1];
        $mysqli->query("INSERT INTO gamesRecord (spieler1, spieler2, toreSpieler1, toreSpieler2, spieltyp) VALUES('$name1','$name2', $tor1, $tor2, 'gewertet')");

    }


}
if (isset($_POST['submitTabelle'])) {

//Spiele in die Datenbank schreiben.
    for ($groupNum = 1; $groupNum <= $gruppenzahl; $groupNum++) {

        for ($i = 1; $i <= count($gruppenMatchups['Gruppe' . $groupNum]); $i++) {

            if (count($gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $i]) == 4)
                for ($p = 0; $p < 4; $p += 2) {

                    $name1 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $i][$p];
                    $name2 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $i][$p + 1];
                    $tor1 = $gruppenTore['Gruppe' . $groupNum]['Spiel' . $i][$p];
                    $tor2 = $gruppenTore['Gruppe' . $groupNum]['Spiel' . $i][$p + 1];
                    $mysqli->query("INSERT INTO gamesRecord (spieler1, spieler2, toreSpieler1, toreSpieler2, spieltyp) VALUES('$name1','$name2', $tor1, $tor2, 'gewertet')");

                }
            else {
                $name1 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $i][0];
                $name2 = $gruppenMatchups['Gruppe' . $groupNum]['Spiel' . $i][1];
                $tor1 = $gruppenTore['Gruppe' . $groupNum]['Spiel' . $i][0];
                $tor2 = $gruppenTore['Gruppe' . $groupNum]['Spiel' . $i][1];
                $mysqli->query("INSERT INTO gamesRecord (spieler1, spieler2, toreSpieler1, toreSpieler2, spieltyp) VALUES('$name1','$name2', $tor1, $tor2, 'gewertet')");
            }

        }
    }
}


$mysqli->close();