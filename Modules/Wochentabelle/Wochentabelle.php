<?php
include("Wochentabelle-Statistik.php");
function showCaption()
{
    global $range;
    $start = $range['start'];
    $end = $range['end'];

    echo "Aktuell: " . date("d D M", strtotime($start)) . " bis " . date("d D M", strtotime($end . "- 1 day"));
}
function generateBestList($wins, $diff)
{
    foreach ($wins as $name => $win) {

    echo
<<<DOC
<tr>
    <td><a href="Profil.php?name=$name">$name</a></td>
    <td>$win</td>
    <td>$diff[$name]</td>
</tr>
DOC;

    }
}

?>

<div class="well table-well">

    <form action="<?php $_SERVER['PHP_SELF'] ?>" method="get">
        <label for="spieltyp">Spieltyp</label>
        <select name="spieltyp" id="spieltyp">
            <option value="normal">Normale Spiele</option>
            <option value="gewertet">Gewertete Spiele</option>
        </select>
        <label for="datum">Datum</label>
        <select name="datum" id="datum">
            <option value="07.09.2015">07.09.2015</option>
            <option value="14.09.2015">14.09.2015</option>
            <option selected value="21.09.2015">21.09.2015</option>
        </select>
        <input type="submit" name="filtern" value="filtern">
    </form>

    <div class="table-responsive">
        <table id="wochentabelle" class="tablesorter table auswertung punktetabelle table-hover">
            <caption><?php showCaption(); ?></caption>
            <thead>
            <tr>
                <th>Spieler</th>
                <th>Gewonnene Spiele</th>
                <th>Tordifferenz &Oslash;</th>
            </tr>
            </thead>
            <tbody>
            <?php generateBestList($wochenwins, $wochenAvgTorDiff); ?>
            </tbody>
        </table>
    </div>
</div>

