<?php

function getStandardValues()
{
    $montag = date("Y-m-d H:i:s", strtotime("Monday this week"));
    $sonntag = date("Y-m-d H:i:s", strtotime("Saturday this week"));
    $range = array("start" => $montag, "end" => $sonntag);
    $spieltyp = "normal";
    return array($range, $spieltyp);
}
function filterGames()
{
    $spieltyp = $_GET['spieltyp'];
    $date = $_GET['datum'];
    $montag = date("Y-m-d H:i:s", strtotime($date));
    $sonntag = date("Y-m-d H:i:s", strtotime($date . "+5 days"));
    $range = array('start' => $montag, 'end' => $sonntag);
    return array($spieltyp, $range);
}
function sortByPoints($wins, $averageGoalDiff) {
    ksort($wins);
    ksort($averageGoalDiff);
    array_multisort($wins, SORT_DESC, $averageGoalDiff, SORT_DESC);

    return array($wins, $averageGoalDiff);
}

list($range, $spieltyp) = getStandardValues();

if (isset($_GET['filtern'])) {
    list($spieltyp, $range) = filterGames();
}

$queryMaster = new SqlQuery($mysqli, $spieltyp, $range);
$playedGames = $queryMaster->getQueryResultForGamesPlayed();
list($goalsPlayer1, $goalsPlayer2) = $queryMaster->getQueryResultsForNumberOfGoals();
list($numberOfGamesPlayer1, $numberOfGamesPlayer2) = $queryMaster->getQueryResultsForNumberOfGames();

$arrayCreator = new ArrayCreator();
$wochenwins = $arrayCreator->createArrayForWonGames($playedGames);
$wochenGameTorDiff = $arrayCreator->createArrayForGoalDifference($goalsPlayer1, $goalsPlayer2);
$wochenGameNumber = $arrayCreator->createArrayForGameNumber($numberOfGamesPlayer1, $numberOfGamesPlayer2);
$wochenAvgTorDiff = $arrayCreator->createArrayForAverageGoalDifference($wochenGameTorDiff, $wochenGameNumber);

list($wochenwins, $wochenAvgTorDiff) = sortByPoints($wochenwins, $wochenAvgTorDiff);