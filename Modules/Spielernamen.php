<?php
function listInputFields($spielerzahl)
{
    for ($i = 1; $i <= $spielerzahl; $i++) {
        echo
        <<<DOC
                <div>
            <label class='name-label' for='spieler$i'>Name $i</label>
            <input type='text' id='spieler$i' name='spieler[]' required>
        </div>
DOC;
    }
}
$spielerzahl = $_GET["spielerzahl"];
?>

<div class="container">
        <div class="well center">

        <?php if($spielerzahl > 1) : ?>
    <form action="<?php $_SERVER['PHP_SELF']?>" method="get">
        <input hidden name="id" value="2">
        <?php listInputFields($spielerzahl); ?>
        <div class="spieler-btn">
            <input type="submit" name="gruppenphase" value="Tabelle generieren!"/>
        </div>
    </form>
<?php else: ?>
    <strong>Bitte Zahl (min. 2) eingeben!</strong>
<?php endif; ?>

</div>
</div>