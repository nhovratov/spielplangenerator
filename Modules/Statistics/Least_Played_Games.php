<?php
$sqlquery = new SqlQuery($mysqli);
list($result1, $result2) = $sqlquery->getQueryResultsForAllGameNumber();
$leastgames = ArrayCreator::createArrayForMostGames($result1, $result2, "ascending");

$winner = key($leastgames);
$text = "Sollte mal wieder spielen";
$content = /** @lang text */
    "<p><a href=\"Profil.php?name=$winner\">$winner</a></p>";

Layout::displayStatBox($text, $content);