<?php
$sqlquery = new SqlQuery($mysqli);
$arrayCreator = new ArrayCreator();
list($result1, $result2) = $sqlquery->getQueryResultsForAllGameNumber();
$gameNumber = ArrayCreator::createArrayForMostGames($result1, $result2);

list($result1, $result2) = $sqlquery->getAllQueryResultsForGoals();
$gameTorDiff = $arrayCreator->createArrayForGoalDifference($result1, $result2);

$avgTorDiff = $arrayCreator->createArrayForAverageGoalDifference($gameTorDiff, $gameNumber);

$winner = key($avgTorDiff);
$stats = current($avgTorDiff);
$text = "Höchste durchschn. Tordifferenz";
$content = /** @lang text */
    "<p><a href='Profil.php?name=$winner' >$winner</a>: $stats</p>";


Layout::displayStatBox($text, $content);