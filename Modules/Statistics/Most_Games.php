<?php
$sqlquery = new SqlQuery($mysqli);
list($result1, $result2) = $sqlquery->getQueryResultsForAllGameNumber();
$gameNumber = ArrayCreator::createArrayForMostGames($result1, $result2);

$winner = key($gameNumber);
$link = "Profil.php?name=$winner";
$text = "Meiste Spiele";
$content = /** @lang text */
    "<p><a href='$link' >$winner</a></p>";

Layout::displayStatBox($text, $content);
