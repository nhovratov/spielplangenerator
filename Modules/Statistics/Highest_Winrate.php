<?php
$sqlquery = new SqlQuery($mysqli);
$arrayCreator = new ArrayCreator();

list($result1, $result2) = $sqlquery->getQueryResultsForAllGameNumber();
$gameNumber = ArrayCreator::createArrayForMostGames($result1, $result2);

$result = $sqlquery->getAllGames();
$winanzahl = $arrayCreator->createArrayForWonGames($result);

$winrate = ArrayCreator::getWinPercentage($winanzahl, $gameNumber);

$winner = key($winrate);
$link = "Profil.php?name=$winner";
$percent = (100) * current($winrate) . " %";
$text = "Höchste Siegesrate";
$content = /**@lang text" */
    "<p><a href='$link' >$winner</a>: $percent </p>";

Layout::displayStatBox($text, $content);