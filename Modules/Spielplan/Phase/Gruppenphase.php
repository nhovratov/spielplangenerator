<form action="<?php $_SERVER['PHP_SELF']?>" method="get">
    <?php for ($i = 1; $i <= count($gruppenMatchups); $i++) : ?>
        <div class="col-lg-6">
            <div class="table-well well ">
                <table class="table table-responsive table-hover">
                    <?php echo"<caption>Gruppe $i</caption>"; ?>
                    <?php $tableManager->createMatchupTableInnerForGroupPhase($gruppenMatchups, $i); ?>
                </table>
            </div>
        </div>
    <?php endfor; ?>
    <div class="col-lg-6">
        <?php echo "<input type='hidden' name='id' value='3' />"?>
        <?php echo "<input type='submit' name='$phase' value='Auswerten!' />" ?>
    </div>
</form>