<?php
session_start();
$players = $_GET["spieler"];

$generator = new MatchupGenerator($players);
$tableManager = new GameTableManager();
$SQL = new SqlQuery($mysqli);

$gruppenMatchups = $generator->getSpreadedMatchups();
$_SESSION['matchups'] = $gruppenMatchups;
$SQL->saveNewPlayer($players);

if(isset($_GET['gruppenphase'])) {
    $phase = "group";
    include("Modules/Spielplan/Phase/Gruppenphase.php");
}

if(isset($_POST['semifinal'])) {
    $phase = "semifinal";
//    $tableManager->displayMatchupTableForSemifinal($tableManager->columns, $players);
}

if(isset($_POST['final'])) {
    $phase = "final";
//    $tableManager->displayMatchupTableForFinal($tableManager->columns, $players);
}