<?php
$sqlquery = new SqlQuery($mysqli);

if (isset($_GET['speichern'])) {
$fail = true;
    if (isset($gamemode)) {
        $fail = false;
        if ($_GET['gamemode'] == "normal") {
            $gamemode = "normal";
        }

        if ($_GET['gamemode'] == "ranked") {
            $gamemode = "gewertet";
        }
    }

    if (isset($_GET['spieler1'])) {
        $fail = false;
        $spieler1 = $_GET['spieler1'];
    }
    if (isset($_GET['spieler2'])) {
        $fail = false;
        $spieler2 = $_GET['spieler2'];
    }
    if (isset($_GET['tore1'])) {
        $fail = false;
        $tore1 = $_GET['tore1'];
    }
    if (isset($_GET['tore2'])) {
        $fail = false;
        $tore2 = $_GET['tore2'];
    }
    if (isset($_GET['date'])) {
        $fail = false;
        $datum = $_GET['date'];
    }

    if(!$fail) {
        $players = [$spieler1, $spieler2];
        $goals = [$tore1, $tore2];
        $sqlquery->saveNewPlayer($players);
        $sqlquery->saveGame($players[0], $players[1], $goals[0], $goals[1], $gamemode, $datum);
    }
}