<?php include "Schnellspeichern.php" ?>

<div class="well center">
    <h2>Schnelleingabe</h2>
    <form action="<?php $_SERVER['PHP_SELF'] ?>" method="get">
        <fieldset>
            <select name="gamemode">
                <option value="normal">Normales Spiel</option>
                <option value="ranked">Gewertetes Spiel</option>
            </select>
            <input type="text" name="date" value="<?php echo date("Y-m-d H:i:s", strtotime("now")) ?>"/>
        </fieldset>

        <fieldset>
            <label class="formular" for="spieler1">Spieler 1</label>
            <input type="text" id="spieler1" name="spieler1" required>
            <label for="tore1">Tore Spieler 1</label>
            <input class="score" type="text" id="tore1" name="tore1" maxlength="2" required>
        </fieldset>

        <fieldset>
            <label class="formular" for="spieler2">Spieler 2</label>
            <input type="text" id="spieler2" name="spieler2" required>
            <label for="tore2">Tore Spieler 2</label>
            <input class="score" type="text" id="tore2" name="tore2" maxlength="2" required>
        </fieldset>

        <input type="submit" name="speichern" value="Spiel eintragen">
    </form>
</div>