<?php

$gamemode = "normal";
$tag1 = date("Y-m-d H:i:s", strtotime("Monday this week"));
$tag2 = date("Y-m-d H:i:s", strtotime("Saturday this week"));
$range = array("start" => $tag1, "end" => $tag2);

if (isset($_GET['filtern'])) {

    $gamemode = $_GET['spieltyp'];
    $datum = $_GET['datum'];
    $tag1 = date("Y-m-d H:i:s", strtotime($datum));
    $tag2 = date("Y-m-d H:i:s", strtotime($datum . "+ 5 days"));
    $range = array("start" => $tag1, "end" => $tag2);
}

$queryMaster = new SqlQuery($mysqli, $gamemode, $range);
$arrayCreator = new ArrayCreator();

$result = $queryMaster->getQueryResultForGamesPlayed();
$lastGames = $arrayCreator->createArrayForLastGames($result);