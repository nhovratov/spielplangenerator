<?php
include "Spielverlauf-Daten.php";
function createMatchHistory($lastGames)
{
    foreach ($lastGames as $last) {
        echo "<tr>
                    <td><a href='../../Profil.php?name=$last[spieler1]'>$last[spieler1]</a></td>
                    <td>$last[tore1] - $last[tore2]</td>
                    <td><a href='../../Profil.php?name=$last[spieler2]'>$last[spieler2]</a></td>
                    <td>$last[spieltyp]</td>
                </tr>";
    }
}
function displayCaption() {
    global $range;
    echo date("d M", strtotime($range['start']));
}
?>
<div class="well table-well">

    <form action="<?php $_SERVER['PHP_SELF'] ?>" method="get">
        <label for="spieltyp2">Spieltyp</label>
        <select name="spieltyp" id="spieltyp2">
            <option value="normal">Normale Spiele</option>
            <option value="gewertet">Gewertete Spiele</option>
            <option value="alles">Alle Spiele</option>
        </select>
        <label for="datum2"></label>
        <select name="datum" id="datum2">
            <option value="07.09.2015">07.09.2015</option>
            <option value="14.09.2015">14.09.2015</option>
            <option selected value="21.09.2015">21.09.2015</option>
        </select>
        <input type="submit" name="filtern" value="filtern">
    </form>

    <div class="table-responsive">
        <table class="table punktetabelle table-hover">
            <caption>Spielverlauf <?php displayCaption() ?></caption>
            <thead>
            <tr>
                <th>Spieler 1</th>
                <th>Score</th>
                <th>Spieler 2</th>
                <th>Spieltyp</th>
            </tr>
            </thead>
            <tbody>
                <?php createMatchHistory($lastGames); ?>
            </tbody>
        </table>
    </div>
</div>