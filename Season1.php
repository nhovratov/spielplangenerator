<?php
require_once("db_daten.php");
$woche = 1;
$result = $mysqli->query("SELECT * FROM gamesRecord WHERE spieltyp = 'season1' AND woche = '$woche'");

while($row = $result->fetch_array()) {
    $gruppenMatchups['Woche1']['Spiel'.$row['spiel']][] = $row['spieler1'];
    $gruppenMatchups['Woche1']['Spiel'.$row['spiel']][] = $row['spieler2'];

}
//echo '<pre>';
//print_r($gruppenMatchups);
//echo '</pre>';
$result = $mysqli->query("SELECT * FROM gamesRecord WHERE spieltyp = 'season1'");

while($row = $result->fetch_array()) {
    $ergebnisse[$row['spiel']][] = $row['toreSpieler1'];
    $ergebnisse[$row['spiel']][] = $row['toreSpieler2'];
}
//echo '<pre>';
//print_r($ergebnisse);
//echo '</pre>';


for($n = 1; $n <= count($gruppenMatchups['Woche'.$woche]); $n++) {
    if(isset($_POST['submit'.$n])) {
//Spiel in die Datenbank schreiben.

        $name1 = $gruppenMatchups['Woche'.$woche]['Spiel'.$n][0];
        $name2 = $gruppenMatchups['Woche'.$woche]['Spiel'.$n][1];
        $tor1 = $_POST['tore'.($n * 2 - 1)];
        $tor2 = $_POST['tore'.($n * 2)];
        $mysqli->query("UPDATE gamesRecord SET toreSpieler1 = '$tor1' , toreSpieler2 =  '$tor2' WHERE spieltyp = 'season1' AND woche = '$woche' AND spiel = '$n'");
        header('location:'.$_SERVER['PHP_SELF']);
    }
}
for($n = 1; $n <= count($gruppenMatchups['Woche'.$woche]); $n++) {
    if(isset($_POST['delete'.$n])) {
//Spiel in die Datenbank schreiben.
        $mysqli->query("UPDATE gamesRecord SET toreSpieler1 = NULL , toreSpieler2 =  NULL WHERE spieltyp = 'season1' AND woche = '$woche' AND spiel = '$n'");
        header('location:'.$_SERVER['PHP_SELF']);
    }
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <title>Spielplan-Generator</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="Resources/Css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="Resources/Css/mein-style.css">
    <link rel="stylesheet" href="Resources/JavaScript/tablesorter-master/themes/blue/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/Resources/JavaScript/tablesorter-master/jquery.tablesorter.js"></script>
</head>

<body>

<div class="startseite container-fluid">
    <div class="container-fluid">
        <a href="index.php">
            <div class="header well">
                <h1>Willkommen zu Nikitas Spielplan-Generator (Beta)</h1>
                <p><em>...welcher auch wirklich funktioniert.</em></p>
            </div>
        </a>
    </div>

<div class="wrapper">
    <div class ="container">
        <div class = "row">


            <div class="col-lg-4">
            <?php
            echo'
            <div class="table-well well ">
            <table class="table table-responsive table-hover season">
            <caption>Season 1 Woche 1</caption>';
            $score = "Score";

            $i = 1;
            for($n = 1; $n <= 9; $n++) {
                $spieltag = "Spiel " . $n;
                echo '
            <!-- Überschriften -->
                    <thead>
                        <tr>
                            <th>' . $spieltag . '</th>
                            <th>&nbsp;</th>
                            <th>' . $score . '</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tr class="scoreRow">
            <!-- Spieler 1 gegen Spieler 2 -->
                        <td>' . $gruppenMatchups['Woche1']['Spiel'.$n][0] . '</td>
                        <td>' . $gruppenMatchups['Woche1']['Spiel'.$n][1] . '</td>

                <!-- Score Felder -->
                        <form action="Season1.php" method="post">
                    <td><input data-name ="'.$gruppenMatchups['Woche1']['Spiel'.$n][0].'" value="'.$ergebnisse[$n][0].'" class="score" type="text" name="tore'.$i.'" maxlength="2" required/>&nbsp;:
                        <input data-name ="'.$gruppenMatchups['Woche1']['Spiel'.$n][1].'" value="'.$ergebnisse[$n][1].'" class="score" type="text" name="tore'.($i + 1).'" maxlength="2" required/>  </td>
                        <td class="add"><input style="width: 30px" type="submit" name="submit'.$n.'" value="+"></td>
                        <td class="del"><input style="width: 30px" type="submit" name="delete'.$n.'" value="-"></td>
                        </form>
                    </tr>';
                $i += 2;
            }
            ?>
            </table>
                </div>
            </div>


<div class="col-lg-8">
<?php

$spieltyp = "season1";

    //Winanzahl
    $result = $mysqli->query("SELECT spieler1, spieler2, toreSpieler1, toreSpieler2 FROM gamesRecord WHERE spieltyp = '$spieltyp' AND woche = '$woche'");

    while($row = $result->fetch_array()) {
        if($row['toreSpieler1'] > $row['toreSpieler2']) {
            $wochenwins[$row['spieler1']] += 1;
            $wochenwins[$row['spieler2']] += 0;
        }
        else if($row['toreSpieler1'] < $row['toreSpieler2']) {

            $wochenwins[$row['spieler2']] += 1;
            $wochenwins[$row['spieler1']] += 0;
        }
    }
    // Spielanzahl berechnen
    $result1 = $mysqli->query("SELECT spieler1, COUNT(*) FROM gamesRecord WHERE spieltyp = '$spieltyp' AND woche = '$woche' AND toreSpieler1 IS NOT NULL GROUP BY spieler1");
    $result2 = $mysqli->query("SELECT spieler2, COUNT(*) FROM gamesRecord WHERE spieltyp = '$spieltyp' AND woche = '$woche' AND toreSpieler2 IS NOT NULL GROUP BY spieler2");

    while($row = $result1->fetch_array()) {
        $wochenGameNumber[$row['spieler1']] += $row[1];
    }

    while($row = $result2->fetch_array()) {
        $wochenGameNumber[$row['spieler2']] += $row[1];
    }

    //Tordifferenzen berechnen und danach den Durchschnitt.

    $result1 = $mysqli->query("SELECT spieler1, sum(toreSpieler1), sum(toreSpieler2) FROM gamesRecord WHERE spieltyp = '$spieltyp' AND woche = '$woche' AND toreSpieler1 IS NOT NULL GROUP BY spieler1");
    $result2 = $mysqli->query("SELECT spieler2, sum(toreSpieler1), sum(toreSpieler2) FROM gamesRecord WHERE spieltyp = '$spieltyp' AND woche = '$woche' AND toreSpieler2 IS NOT NULL GROUP BY spieler2");

    while($row = $result1->fetch_array()) {
        $wochenGameTorDiff[$row['spieler1']] += $row['sum(toreSpieler1)'] - $row['sum(toreSpieler2)'];
    }
    while($row = $result2->fetch_array()) {
        $wochenGameTorDiff[$row['spieler2']] += ($row['sum(toreSpieler2)'] - $row['sum(toreSpieler1)']);
    }

    foreach($wochenGameTorDiff as $name => $diff) {

        $wochenAvgTorDiff[$name] = round($diff/$wochenGameNumber[$name], 1);
    }

    foreach($wochenwins as $name => $punkte) {
        $wochendiff[$name] = $wochenAvgTorDiff[$name];
    }
    //echo'<pre>';
    //print_r($wochenAvgTorDiff);
    //echo'</pre>';

    array_multisort($wochenwins, SORT_DESC, $wochendiff, SORT_DESC);



    ?>

    <div class="well table-well" style="height: 350px; overflow-y: scroll;">

            <div class="table-responsive">
                <table id="wochentabelle" class="tablesorter table auswertung punktetabelle table-hover">
                    <caption>Woche 1</caption>
                    <thead>
                    <tr>
                        <th>Spieler</th>
                        <th>Gewonnene Spiele</th>
                        <th>Tordifferenz &Oslash;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    foreach($wochenwins as $name => $wins) {
                        echo '<tr>
                                    <td><a href="Profil.php?name='.$name.'">'.$name.'</a></td>
                                    <td>'.$wins.'</td>
                                    <td>'.$wochendiff[$name].'</td>
                                  </tr>';
                        $i++;
                    }
                    ?>
                    </tbody>
                </table>
            </div>
    </div>



</div>


</div>

</div>
</div>
</div>
<script>
$(document).ready(function() {
    $("[type='text']").each(function(){
        console.log($(this).val());
            if($(this).val() != "") {
                $(this).attr("disabled", "disabled");
                $(this).parents("tr").children(".add").children().attr("disabled", "disabled");
            }
        });

    });
</script>
</body>
</html>
