<?php
require_once('db_daten.php');

$name = $_GET['name'];

$result = $mysqli->query("SELECT spieler1, spieler2, count(*)  FROM gamesRecord WHERE datum IS NOT NULL AND (spieler1 = '$name' OR spieler2 = '$name')");
$spielanzahl = $result->fetch_array();

$result = $mysqli->query("SELECT spieler1, spieler2, toreSpieler1, toreSpieler2 FROM gamesRecord WHERE datum IS NOT NULL AND (spieler1 = '$name' OR spieler2 = '$name') ");

$tied=0;
while($row = $result->fetch_array()) {

    if($row["spieler1"] == $name) {
        if ($row["toreSpieler1"] > $row["toreSpieler2"]) {
            $wins += 1;
            $punkte += 3;
        }
        if ($row['toreSpieler1'] == $row['toreSpieler2']) {
            $tied += 1;
        }
        $eigeneTordiff += $row["toreSpieler1"] - $row["toreSpieler2"];
        $rivale[$row['spieler2']] += $row["toreSpieler1"] - $row["toreSpieler2"];

    }
    else {
        if ($row["toreSpieler1"] < $row["toreSpieler2"]) {
            $wins += 1;
            $punkte += 3;
        }
        if ($row['toreSpieler1'] == $row['toreSpieler2']) {
            $tied += 1;
        }
        $eigeneTordiff += $row["toreSpieler2"] - $row["toreSpieler1"];
        $rivale[$row['spieler1']] += $row["toreSpieler2"] - $row["toreSpieler1"];
    }
}
asort($rivale);
$winrate = 100 * round(($wins/$spielanzahl['count(*)']), 2);

?>




<!DOCTYPE html>
<html lang="de">
<head>
    <title>Spielplan-Generator</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="Resources/Css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="Resources/Css/mein-style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<div class="startseite container-fluid">
    <div class="container-fluid">
        <a href="index.php">
            <div class="header well">
                <h1>Willkommen zu Nikitas Spielplan-Generator (Beta)</h1>
                <p><em>...welcher auch wirklich funktioniert.</em></p>
            </div>
        </a>
    </div>

    <div class="container center">
        <div class="well">
            <h2>Profil von <?php echo $name ?></h2>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="center infopanel panel panel-default">
                    <div class="panel-heading">
                        <h3>Spielstatistik</h3>
                    </div>
                    <div class="panel-body">
                     <p class="startinfo"><?php echo 'Spiele gesamt: '.$spielanzahl['count(*)'].'<br>
                                                      Gewonnen: '.$wins.'
                                                      <br> Verloren: '.($spielanzahl['count(*)'] - $wins).'
                                                      <br> Unentschieden: '.$tied; ?></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="center infopanel panel panel-default">
                    <div class="panel-heading">
                        <h3>Siegesrate</h3>
                    </div>
                    <div class="panel-body">
                        <p class="startinfo"><?php echo $winrate."%"; ?></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="center infopanel panel panel-default">
                    <div class="panel-heading">
                        <h3>Gesamtpunktzahl</h3>
                    </div>
                    <div class="panel-body">
                        <p class="startinfo"><?php echo $punkte; ?></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="center infopanel panel panel-default">
                    <div class="panel-heading">
                        <h3>Tordifferenz</h3>
                    </div>
                    <div class="panel-body">
                        <p class="startinfo"><?php echo $eigeneTordiff; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
            <div class="well table-well">
                <table class="table table-responsive table-hover">
                    <caption>Spielverlauf</caption>
                    <?php

                     $result = $mysqli->query("SELECT spieler1, spieler2, toreSpieler1, toreSpieler2 FROM gamesRecord
                                               WHERE datum IS NOT NULL AND (spieler1 = '$name' OR spieler2 = '$name') ORDER BY gameId DESC LIMIT 8");

                    $i = 0;
                    while($row = $result->fetch_array()) {

                            if($row['spieler1'] == $name) {
                                echo'
                                <tr class="bordertop">
                                    <td><a href="Profil.php?name='.$row['spieler1'].'">'. $row['spieler1'].'</a></td>
                                    <td> Vs. </td>
                                    <td><a href="Profil.php?name='.$row['spieler2'].'">'. $row['spieler2'].'</a></td>
                                </tr>
                                <tr>
                                    <td>'. $row['toreSpieler1'].'</td>
                                    <td></td>
                                    <td>'. $row['toreSpieler2'].'</td>
                                </tr>';
                            }
                            else {
                                echo'
                                <tr class="bordertop">
                                    <td><a href="Profil.php?name='.$row['spieler2'].'">'. $row['spieler2'].'</a></td>
                                    <td> Vs. </td>
                                    <td><a href="Profil.php?name='.$row['spieler1'].'">'. $row['spieler1'].'</a></td>
                                </tr>
                                <tr>
                                    <td>'. $row['toreSpieler2'].'</td>
                                    <td></td>
                                    <td>'. $row['toreSpieler1'].'</td>
                                </tr>';
                            }
                            $i++;


                    }
                    ?>

                    </tbody>
                </table>
            </div>
            </div>
            <div class="col-lg-3">
                <div class="center infopanel panel panel-default">
                    <div class="panel-heading">
                        <h3>Erzrivale</h3>
                    </div>
                    <div class="panel-body">
                        <a href="Profil.php?name=<?php echo key($rivale);?>"><p class="startinfo"><?php echo key($rivale); ?></p></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div><!-- ende startseite -->
</body>
</html>