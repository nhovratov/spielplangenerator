<?php


class ArrayCreator
{
    private $player1 = "spieler1";
    private $player2 = "spieler2";

    public function __construct()
    {

    }

    public function getStandardValues()
    {
        $montag = date("Y-m-d H:i:s", strtotime("Monday this week"));
        $sonntag = date("Y-m-d H:i:s", strtotime("Saturday this week"));
        $range = array("start" => $montag, "end" => $sonntag);
        $spieltyp = "normal";
        return array($range, $spieltyp);
    }

    private function fetchGameNumber($numberOfGames, $player, $gameNumber = array())
    {
        while ($row = mysqli_fetch_array($numberOfGames)) {
            $gameNumber[$row[$player]] += $row[1];
        }
        return $gameNumber;
    }

    private function fetchGoalDifference($result, $player, $goalDifference = array())
    {
        while ($row = mysqli_fetch_array($result)) {
            if ($player == "spieler1") {
                $goalDifference[$row[$player]] += $row['sum(toreSpieler1)'] - $row['sum(toreSpieler2)'];
            } else {
                $goalDifference[$row[$player]] += $row['sum(toreSpieler2)'] - $row['sum(toreSpieler1)'];
            }
        }

        return $goalDifference;
    }

    public function createArrayForWonGames($playedGames)
    {
        $wins = array();
        while ($row = mysqli_fetch_array($playedGames)) {

            if ($row['toreSpieler1'] > $row['toreSpieler2']) {
                $wins[$row['spieler1']] += 1;
                $wins[$row['spieler2']] += 0;
            } else {
                $wins[$row['spieler2']] += 1;
                $wins[$row['spieler1']] += 0;
            }
        }
        return $wins;
    }

    public function createArrayForGameNumber($result1, $result2)
    {
        $gameNumber = array();
        $gameNumber = $this->fetchGameNumber($result1, $this->player1, $gameNumber);
        $gameNumber = $this->fetchGameNumber($result2, $this->player2, $gameNumber);

        return $gameNumber;
    }

    public function createArrayForGoalDifference($result1, $result2, $goalDifference = array())
    {
        $goalDifference = $this->fetchGoalDifference($result1, $this->player1, $goalDifference);
        $goalDifference = $this->fetchGoalDifference($result2, $this->player2, $goalDifference);
        arsort($goalDifference);
        return $goalDifference;
    }

    public function createArrayForAverageGoalDifference($goalDifference, $gameNumber, $averageDifference = array())
    {
        foreach ($goalDifference as $name => $diff) {
            $num = $gameNumber[$name];
            $averageDifference[$name] = round($diff / $num, 1);
        }
        arsort($averageDifference);
        return $averageDifference;
    }

    public function createArrayForLastGames($result, $lastGames = array())
    {
        while ($row = mysqli_fetch_array($result)) {
            $lastGames[] = array(
                'spieler1' => $row['spieler1'],
                'spieler2' => $row['spieler2'],
                'tore1' => $row['toreSpieler1'],
                'tore2' => $row['toreSpieler2'],
                'spieltyp' => $row['spieltyp']
            );
        }
        return $lastGames;
    }

    public static function createArrayForMostGames($result1, $result2, $sort = "desc") {
        $gameNumber = [];
        while($row = $result1->fetch_array()) {
            $gameNumber[$row['spieler1']] += $row[1];
        }

        while($row = $result2->fetch_array()) {
            $gameNumber[$row['spieler2']] += $row[1];
        }

        $sort == "desc" ? arsort($gameNumber) : asort($gameNumber);
        return $gameNumber;
    }

    public static function getWinPercentage(array $numberOfWins, array $gameNumber, $winrate = []) {

        foreach($numberOfWins as $name => $win) {
            $winrate[$name] = round($win/$gameNumber[$name], 2);
        }
        arsort($winrate);
        return $winrate;
    }

}