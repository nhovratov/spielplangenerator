<?php

class MatchupGenerator
{
    const GROUP_DIVIDER = 5;

    private $players = [];
    private $playerCount = 0;
    private $groupCount = 0;
    private $abstractDistribution = [];
    private $playerDistribution = [];
    private $matchups = [];
    private $spreadedMatchups = [];

    public function __construct($players)
    {
        $this->players = $this->shufflePlayers($players);
        $this->playerCount = $this->countPlayer($players);
        $this->groupCount = $this->calculateGroupNumber();
        $this->abstractDistribution = $this->dividePlayersIntoGroups($this->groupCount, $this->playerCount);
        $this->playerDistribution = $this->slicePlayers($this->groupCount);
        $this->matchups = $this->generateAllPossibleMatchups();
        $this->spreadedMatchups = $this->spreadMatchups();
    }

    private function shufflePlayers($players)
    {
        shuffle($players);
        return $players;

    }

    private function countPlayer($players)
    {
        $num = count($players);
        return $num;
    }

    private function calculateGroupNumber()
    {
        return ceil($this->playerCount / self::GROUP_DIVIDER);
    }

    private function dividePlayersIntoGroups($remainingGroups, $remainingPlayers, $distribution = array())
    {
        if ($remainingGroups == 0) {
            return $distribution;
        }

        $average = $remainingPlayers / $remainingGroups;

        if ($remainingPlayers % $remainingGroups != 0) {
            $distribution[] = ceil($average);
            $remainingPlayers -= ceil($average);
            $remainingGroups--;
            return $this->dividePlayersIntoGroups($remainingGroups, $remainingPlayers, $distribution);

        } else {
            $distribution[] = floor($average);
            $remainingPlayers -= floor($average);
            $remainingGroups--;
            return $this->dividePlayersIntoGroups($remainingGroups, $remainingPlayers, $distribution);
        }

    }

    private function slicePlayers($groupCount, $start = 0, $i = 1, $playerDistribution = array())
    {
        if ($groupCount == 0) {
            return $playerDistribution;
        }
        $index = $i - 1;
        $playerDistribution["Gruppe$i"] = array_slice($this->players, $start, $this->abstractDistribution[$index]);
        $start += $this->abstractDistribution[$index];
        $groupCount--;
        $i++;
        return $this->slicePlayers($groupCount, $start, $i, $playerDistribution);
    }

    private function generateMatchups($groupNum, $remainingEnemies, $groups = array(), $index1 = 0, $index2 = 1, $count = 1)
    {

        if ($remainingEnemies == 0) {
            return $groups;
        }

        for ($i = 1; $i <= $remainingEnemies; $i++, $index2++) {
            $groups["Gruppe$groupNum"]["Spiel$count"] = array(
                $this->playerDistribution["Gruppe$groupNum"][$index1],
                $this->playerDistribution["Gruppe$groupNum"][$index2]
            );
            $count++;
        }

        $index1++;
        $index2 = $index1 + 1;
        $remainingEnemies--;
        return $this->generateMatchups($groupNum, $remainingEnemies, $groups, $index1, $index2, $count);
    }

    private function generateAllPossibleMatchups($groups = array())
    {
        for ($groupNum = 1; $groupNum <= $this->groupCount; $groupNum++) {

            $playerCount = count($this->playerDistribution["Gruppe$groupNum"]);
            $remainingEnemies = $playerCount - 1;
            $groups = $this->generateMatchups($groupNum, $remainingEnemies, $groups);

        }
        return $groups;
    }

    private function spreadMatchups($gruppenMatchups = array())
    {
        for ($i = 1; $i <= $this->groupCount; $i++) {
            $index2 = count($this->matchups["Gruppe$i"]);
            $gruppenMatchups = $this->spreadGroup($i, $index2, $gruppenMatchups);
        }

        return $gruppenMatchups;
    }

    private function spreadGroup($i, $index2, $gruppenMatchups, $j = 1, $index1 = 1, $game = 1)
    {
        $numberOfIterations = ceil(count($this->matchups["Gruppe$i"]) / 2) + 1;
        if ($j == $numberOfIterations) {
            return $gruppenMatchups;
        }

        $player1 = $this->matchups["Gruppe$i"]["Spiel$index1"][0];
        $player2 = $this->matchups["Gruppe$i"]["Spiel$index1"][1];
        $player3 = $this->matchups["Gruppe$i"]["Spiel$index2"][0];
        $player4 = $this->matchups["Gruppe$i"]["Spiel$index2"][1];

        $gruppenMatchups["Gruppe$i"]["Spiel$game"][] = $player1;
        $gruppenMatchups["Gruppe$i"]["Spiel$game"][] = $player2;

        if ($player1 != $player3 || $player2 != $player4) {
            $gruppenMatchups["Gruppe$i"]["Spiel" . ($game + 1)][] = $player3;
            $gruppenMatchups["Gruppe$i"]["Spiel" . ($game + 1)][] = $player4;
        }

        $index1++;
        $index2--;
        $j++;
        $game += 2;
        return $this->spreadGroup($i, $index2, $gruppenMatchups, $j, $index1, $game);
    }

    /**
     * @return array
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * @return int
     */
    public function getPlayerCount()
    {
        return $this->playerCount;
    }

    /**
     * @return int
     */
    public function getGroupCount()
    {
        return $this->groupCount;
    }

    /**
     * @return array
     */
    public function getPlayerDistribution()
    {
        return $this->playerDistribution;
    }

    /**
     * @return array
     */
    public function getMatchups()
    {
        return $this->matchups;
    }

    /**
     * @return array
     */
    public function getSpreadedMatchups()
    {
        return $this->spreadedMatchups;
    }


}