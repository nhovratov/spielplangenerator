<?php

/**
 * Created by PhpStorm.
 * User: Nikita_Hovratov
 * Date: 11.02.2016
 * Time: 11:53
 */
class Layout
{
    public static function displayHeader()
    {
        echo <<<DOC
<!DOCTYPE html>
<html lang="de">
<head>
    <title>Spielplan-Generator</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="Resources/Css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="Resources/Css/mein-style.css">
    <link rel="stylesheet" href="Resources/JavaScript/tablesorter-master/themes/blue/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="Resources/JavaScript/tablesorter-master/jquery.tablesorter.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/JavaScript/bootstrap.min.js"></script>

DOC;
        echo <<<DOC
</head>
<body>

<div class="startseite container-fluid">
    <div class="container-fluid">
DOC;
        echo <<<DOC
<a href="$_SERVER[PHP_SELF]?id=0">
    <div class="header well">
        <h1>Willkommen zu Nikitas Spielplan-Generator (Beta)</h1>
        <p><em>...welcher auch wirklich funktioniert.</em></p>
    </div>
</a>
DOC;
        echo <<<DOC
</div>
DOC;
    }

    public static function displayFooter()
    {
        echo <<<DOC
    </div>
    <script src="Resources/JavaScript/realtime_results.js"></script>
    <script src="Resources/JavaScript/init_tablesorter.js"></script>
</body>
</html>
DOC;
    }

    public static function displayStatBox($text, $content)
    {
        echo /** @lang text */
        "<div class=\"infopanel panel-default panel\">
            <div class=\"panel-heading\">
                <h3>$text</h3>
            </div>
            <div class=\"panel-body padtop\">
                $content
            </div>
        </div>";
    }
}