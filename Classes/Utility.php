<?php

class Utility
{
    public function __construct()
    {
    }

    public static function printVar($var)
    {
        echo '<pre>';
        print_r($var);
        echo '</pre>';
    }

    public static function dumpVar($var) {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }
}