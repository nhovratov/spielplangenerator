<?php


class GameTableManager
{

    private function getScoreInput($player) {
        return <<<DOC
        <input data-name="$player" class="score" type="text" name="tore[]" maxlength="2" required/>
DOC;

    }

    private function displayTHead() {
        echo  <<<DOC
                <thead>
                    <tr>
                        <th></th>
                        <th>&nbsp;</th>
                        <th>Score</th>
                    </tr>
                </thead>
DOC;
    }

    public function createMatchupTBody($matchups, $i) {
        echo "<tbody>";
        foreach ($matchups["Gruppe$i"] as $game) :
            $player1 = $game[0];
            $player2 = $game[1];
            $score1 = $this->getScoreInput($player1);
            $score2 = $this->getScoreInput($player2);
            echo <<<DOC
                    <tr class="scoreRow">
                        <td>$player1</td>
                        <td>$player2</td>
                        <td>$score1&nbsp;:$score2</td>
                    </tr>
DOC;
        endforeach;
        echo "</tbody>";
    }

    public function displayMatchupTBody($spielernamen, $index) {
        echo <<<DOC
            <tbody>
            <tr>
                <td>$spielernamen[$index]</td>
                <td>{$spielernamen[$index+1]}</td>
                <td><input class="score" type="text" name="tore[]" maxlength="2" required/>&nbsp;:
                <input class="score" type="text" name="tore[]" maxlength="2" required/></td>
            </tr>
            </tbody>
DOC;
    }

    public function createMatchupTableInnerForGroupPhase($matchups, $i) {
            $this->displayTHead();
            $this->createMatchupTBody($matchups, $i);
    }

    public function displayMatchupTableInnerForSemifinal($players) {
        for($index = 0, $groupNum = 1; $index < 4; $index += 2, $groupNum++) {
            $this->displayTHead();
            $this->displayMatchupTBody($players, $index);
        }
    }

    public function displayMatchupTableInnerForFinal($players) {
        for($index = 0, $groupNum = 1; $index < 2; $index += 2, $groupNum++) {
            $this->displayTHead();
            $this->displayMatchupTBody($players, $index);
        }
    }

    public function displayMatchupTableForSemifinal($columns, $players) {
        $this->displayMatchupTableInnerForSemifinal($players);
    }
    
    public function displayMatchupTableForFinal($columns, $players) {
        $this->displayMatchupTableInnerForFinal($players);
    }
}