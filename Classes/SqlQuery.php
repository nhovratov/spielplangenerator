<?php


class SqlQuery
{
    private $mysqli = NULL;
    private $player1 = "spieler1";
    private $player2 = "spieler2";
    private $gameType = "";
    private $range = array();

    public function __construct($mysqli, $gameType = "", $range = array()) {
        $this->mysqli = $mysqli;
        $this->gameType = $gameType;
        $this->range = $range;
    }

    private function getStartEnd() {
        $start = $this->range['start'];
        $end = $this->range['end'];
        return array($start, $end);
    }

    public function getQueryResultForGamesPlayed()
    {
        list($start, $end) = $this->getStartEnd();
        $result = $this->mysqli->query("  SELECT spieler1, spieler2, toreSpieler1, toreSpieler2, spieltyp
                                    FROM gamesRecord
                                    WHERE datum IS NOT NULL
                                    AND spieltyp = '$this->gameType'
                                    AND datum BETWEEN '$start' AND '$end'
                                    ORDER BY gameId DESC");
        return $result;
    }

    public function getAllGames() {
        $result = $this->mysqli->query("SELECT spieler1, spieler2, toreSpieler1, toreSpieler2 FROM gamesRecord WHERE datum IS NOT NULL");
        return $result;
    }

    public function getQueryResultForNumberOfGames($player) {
        list($start, $end) = $this->getStartEnd();
        $result = $this->mysqli->query("  SELECT $player, COUNT(*)
                                    FROM gamesRecord
                                    WHERE datum IS NOT NULL
                                    AND spieltyp = '$this->gameType'
                                    AND datum BETWEEN '$start' AND '$end'
                                    GROUP BY $player");
        return $result;
    }

    public function getQueryResultsForNumberOfGames()
    {
        $result1 = $this->getQueryResultForNumberOfGames($this->player1);
        $result2 = $this->getQueryResultForNumberOfGames($this->player2);

        return array($result1, $result2);
    }

    public function getQueryResultForNumberOfGoals($player) {
        list($start, $end) = $this->getStartEnd();
        $result = $this->mysqli->query("  SELECT $player, sum(toreSpieler1), sum(toreSpieler2)
                                    FROM gamesRecord
                                    WHERE datum IS NOT NULL
                                    AND spieltyp = '$this->gameType'
                                    AND datum BETWEEN '$start' AND '$end'
                                    GROUP BY $player");
        return $result;
    }

    public function getQueryResultsForNumberOfGoals()
    {
        $result1 = $this->getQueryResultForNumberOfGoals($this->player1);
        $result2 = $this->getQueryResultForNumberOfGoals($this->player2);

        return array($result1, $result2);
    }

    public function saveNewPlayer($players, $existingPlayers = array()) {

        $result = $this->mysqli->query("SELECT spielerName FROM Spieler");

        while($row = mysqli_fetch_array($result)) {
            $existingPlayers[] = $row['spielerName'];
        }

        foreach ($players as $player) {
            if(!in_array($player, $existingPlayers)){
                $this->mysqli->query("INSERT INTO Spieler (spielerName) VALUES ('$player') ");
            }
        }
    }

    public function getQueryResultForNewestPlayer() {
        $result = $this->mysqli->query("SELECT spielerName FROM Spieler ORDER BY spielerID DESC LIMIT 1;");
        return $result;
    }

    private function getQueryResultForAllGameNumber($player) {
        $result = $this->mysqli->query("SELECT $player, COUNT(*) FROM gamesRecord WHERE datum IS NOT NULL GROUP BY $player");
        return $result;
    }

    public function getQueryResultsForAllGameNumber() {
        $result1 = $this->getQueryResultForAllGameNumber("spieler1");
        $result2 = $this->getQueryResultForAllGameNumber("spieler2");
        return [$result1, $result2];
    }

    public function getAllQueryResultsForGoals() {
        $result1 = $this->mysqli->query("SELECT spieler1, sum(toreSpieler1), sum(toreSpieler2) FROM gamesRecord WHERE datum IS NOT NULL GROUP BY spieler1");
        $result2 = $this->mysqli->query("SELECT spieler2, sum(toreSpieler1), sum(toreSpieler2) FROM gamesRecord WHERE datum IS NOT NULL GROUP BY spieler2");

        return [$result1, $result2];
    }

    public function getPlayer($spieler) {
        $result = $this->mysqli->query("SELECT spielerName FROM Spieler WHERE spielerName = '$spieler'");
        return $result;
    }

    public function saveGame($spieler1, $spieler2, $tore1, $tore2, $gamemode, $datum) {
        $this->mysqli->query("INSERT INTO gamesRecord (spieler1, spieler2, toreSpieler1, toreSpieler2, spieltyp, datum)
                              VALUES('$spieler1','$spieler2', $tore1, $tore2, '$gamemode', '$datum')");
    }

}