<div class="container">
    <div class="row">

        <div class="col-lg-8">
            <?php include "Modules/Wochentabelle/Wochentabelle.php"; ?>
        </div>

        <div class="col-lg-4">
            <?php include "Modules/Matchhistory/Spielverlauf.php"; ?>
        </div>

    </div>

    <div class="row">

        <div class="col-lg-6">
            <?php include "Modules/Spielerzahl.php"; ?>
        </div>

        <div class="col-lg-6">
            <?php include "Modules/QuickSave/Schnelleingabe.php"; ?>
        </div>
    </div>
</div>

<section class="center">
    <div class="container">
        <div class="row">

            <div class="col-lg-3">
                <?php include "Modules/Statistics/Most_Games.php"; ?>
            </div>

            <div class="col-lg-3">
                <?php include "Modules/Statistics/Highest_Winrate.php"; ?>
            </div>

            <div class="col-lg-3">
                <?php include "Modules/Statistics/Highest_Avg_Diff.php"; ?>
            </div>

            <div class="col-lg-3">
                <?php include "Modules/Statistics/Least_Played_Games.php"; ?>
            </div>

        </div>
    </div>

</section>