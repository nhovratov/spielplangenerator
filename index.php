<?php
include_once("db_daten.php");
include_once("Classes/Layout.php");
include_once("Classes/Utility.php");
include_once("Classes/SqlQuery.php");
include_once("Classes/ArrayCreator.php");
include_once("Classes/GameTableManager.php");
include_once("Classes/MatchupGenerator.php");

$pageId = "";

if (isset($_GET['id'])) {
    $pageId = $_GET['id'];
}

Layout::displayHeader();

switch ($pageId) {
    case "1":
        include_once("Templates/Spielernamen.php");
        break;
    case "2":
        include_once("Templates/Spielplan.php");
        break;
    case "3":
        include_once("Modules/Auswertung/Auswertung.php");
        break;
    default:
        include_once("Templates/Startseite.php");
}

Layout::displayFooter();