<?php
$servername = "localhost:3306";
$username = "root";
$password = "";
$db_name = "spielplangenerator";

// Create connection
$mysqli = new mysqli($servername, $username, $password, $db_name);

// Check connection
if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
}