<!DOCTYPE html>
<html>
<body>
<?php
include_once "../Classes/MatchupGenerator.php";
include_once "../Classes/GameTableManager.php";
$players = ["Nikita", "Jens", "Edgar", "Lars", "Martin", "Tim", "Lisa"];
$phase = "test";

$generator = new MatchupGenerator($players);
$tableManager = new GameTableManager();

$gruppenMatchups = $generator->getSpreadedMatchups();


include("../Modules/Spielplan/Phase/Gruppenphase.php");
?>
</body>
</html>
